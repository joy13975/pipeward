# -*- coding: utf-8 -*-
import socket
import threading
import time

import argparse

class TcpServer(object):
    def __init__(self, host='0.0.0.0', port=1337):
        self.host = host
        self.port = port
        self.should_stop = False
        self.listening = False
        self.retry_listen = 3  # Seconds
        self.handlers = []

    def listen(self, maxbacklog=16):
        self.listening = False
        try:
            server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            server.bind((self.host, self.port))
            server.listen(maxbacklog)
            self.listening = True
            print('Listening on %d' % self.port)

            self.handlers = []
            while not self.should_stop:
                client_sock, address = server.accept()
                print('Accepted connection from {}:{}'.format(address[0], address[1]))
                client_handler = threading.Thread(
                    target=self._handle_client_connection,
                    args=(client_sock,)
                )
                self.handlers.append((client_handler, client_sock))
                client_handler.start()

            server.close()
            self.listening = False
            print('Stopped listening')
        except socket.error, inst:
            print('Could not listen on %s:%d...\nReason: %s' %
            (self.host, self.port, repr(inst)))
            print('Retrying in %ds...' % self.retry_listen)
            time.sleep(self.retry_listen)
        finally:
            print('Waiting for handlers to end...')
            for handler, sock in self.handlers:
                try:
                    sock.shutdown(socket.SHUT_WR)
                except Exception:
                    pass
                handler.join()
            print('Handlers ended')

    def stop(self):
        self.should_stop = True
        client = None
        try:
            client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            client.connect((self.host, self.port))
        except Exception:
            pass
        finally:
            if client:
                client.close()

    def _handle_client_connection(self, client_socket):
        try:
            while not self.should_stop:
                request = client_socket.recv(1024)
                print('Received {}'.format(request))
                client_socket.send('ACK! 2.0')
        except socket.error, inst:
            print('Handler msg: %s' % inst[1])
        finally:
            if client_socket:
                client_socket.close()
            print('Handler exit')

if __name__ == '__main__':
    ap = argparse.ArgumentParser(description='Pipe stuff through file.')
    ap.add_argument('--port', type=int, default=1337)
    args = ap.parse_args()
    serv = TcpServer(port=args.port)
    serv.listen()