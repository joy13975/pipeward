# -*- coding: utf-8 -*-

import threading
from random import random

import argparse

from tcp_client import TcpClient
from tcp_server import TcpServer


class Pipeward(object):
    def __init__(self, port=53):
        self.port = port
        self.tcpserv = TcpServer(port=self.port)
        self.tcpclient = TcpClient()
        self.server_thread = None

    def start(self):
        self.server_thread = threading.Thread(target=self.tcpserv.listen)
        self.server_thread.start()
        self.tcpclient.connect('localhost', self.port)

    def stop(self):
        self.tcpclient.stop()
        if self.tcpserv:
            self.tcpserv.stop()

def get_random_port(minport=1337, maxport=65535):
    portrange = maxport - minport
    port = minport + int(portrange * random())
    return port

if __name__ == '__main__':
    ap = argparse.ArgumentParser(description='Pipe stuff through file.')
    ap.add_argument('--tcpport', type=int, default=get_random_port())
    args = ap.parse_args()
    pw = Pipeward(port=args.tcpport)
    pw.start()