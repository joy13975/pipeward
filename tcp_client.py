# -*- coding: utf-8 -*-
import socket

class TcpClient(object):
    def __init__(self):
        self.client = None

    def connect(self, host, port):
        self.client = None
        try:
            self.client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.client.connect((host, port))
        except Exception, inst:
            print('Could not connect...\nReason: ' + repr(inst))
            self.client.close()
            self.client = None
            return False

        return True


    def _check_connection(method):
        def magic(self, *args, **kwargs):
            if self.client:
                return method(self, *args, **kwargs)
            else:
                print('Client not connected')
                return None
        return magic

    @_check_connection
    def stop(self):
        self.client.close()
        self.client = None
        
    @_check_connection
    def send(self, data):
        return self.client.send(data)

    @_check_connection
    def recv(self, data):
        return self.client.recv(4096)

    def _decorator(foo):
        def magic( self ) :
            print "start magic"
            foo( self )
            print "end magic"
        return magic

if __name__ == '__main__':
    t = TcpClient()
    t.connect('localhost', 1337)